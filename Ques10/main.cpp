/*
//Project Euler #10
//https://projecteuler.net/problem=10
*/

#include <iostream>
#include <conio.h>

bool IsPrime(long long var)
{
	if(var == 2)
		return true;

	for(long long i = 2; i <= var / 2; i++)
	{
		if(var % i == 0)
			return false;
	}

	return true;
}

long long SumOfPrime(long long limit)
{
	long long sum = 0;
	
	for(long long i = 2; i < limit; i++)
	{
		if(IsPrime(i))
		{
			sum += i;
		}
	}

	return sum;
}

int main()
{
	std::cout << "Sum of Primes " << SumOfPrime(2000000);

	_getch();
	return 0;
}