#include <iostream>
#include <conio.h>
#include <time.h>

int CollatzSequence(unsigned int max)
{
	unsigned int num = 1;
	unsigned int longestChainNum = 0;
	
	int chain = 1;
	int longestChain = 0;

	unsigned int tempNum = num;

	while(num < max)
	{
		while (tempNum > 1)
		{
			tempNum = tempNum % 2 == 0 ? tempNum / 2 : (tempNum * 3) + 1;
			chain++;

			if(tempNum == 1)
			{
				if(chain > longestChain)
				{
					longestChainNum = num;
					longestChain = chain;
				}
			}
		}

		num ++;
		tempNum = num;
		chain = 1;
	}

	return longestChainNum;
}

int main()
{
	time_t startTime = time(NULL);

	std::cout << "Collatz Sequence " << CollatzSequence(1000000);
	std::cout << "\n\nTime taken " << time(NULL) - startTime;

	_getch();
	return 0;
}