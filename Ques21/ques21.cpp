#include <iostream>
#include <conio.h>
#include <algorithm>
#include <vector>

using namespace std;

int SumOfMultiple(int num)
{
	int sum = 1;
    for(int i = 2; i * i <= num; sum += num % i ? 0 : i * i == num ? i : i + num / i, i++);
    return sum;
}

int SumOfAmicableNumber(int max)
{
	int sum = 0;
	vector<int> ignoredNumbers = vector<int>();

	for(int i = 1; i < max; i++)
	{
		if(!ignoredNumbers.empty() && find(ignoredNumbers.begin(), ignoredNumbers.end(), i) != ignoredNumbers.end())
			continue;

		int sumOfMultiples = SumOfMultiple(i);
		
		if(SumOfMultiple(sumOfMultiples) == i && sumOfMultiples != i)
		{
			sum += i + sumOfMultiples;
			ignoredNumbers.push_back(sumOfMultiples);
			cout << i << ", " << sumOfMultiples << endl;
		}
	}

	return sum;
}

int main()
{
	cout << "Sum Of Amicable Number " << SumOfAmicableNumber(10000);

	_getch();
	return 0;
}