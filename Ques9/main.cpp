/*
//Project Euler #9
//https://projecteuler.net/problem=9
*/

#include <iostream>
#include <conio.h>
#include <time.h>

int PythagorasTriplet(int sum)
{
	for(int i = 1; i < 1000; i++)
	{
		for(int j = 1; j < 1000; j++)
		{
			for(int k = 1; k < 1000; k++)
			{
				if((i * i) + (j * j) == (k * k))
				{
					if(i + j + k == sum)
					{
						return i * j * k;
					}
				}
			}
		}
	}

	return 0;
}

int main()
{
	time_t startTime = time(NULL);
	
	std::cout << "Pythagoras Triplets " << PythagorasTriplet(1000);
	std::cout << "\n\nTime taken " << time(NULL) - startTime;

	_getch();
	return 0;
}