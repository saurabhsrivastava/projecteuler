#include <iostream>
#include <conio.h>

int Difference(int max)
{
	int sumOfSquare = 0;
	int squareofSum = 0;

	for(int i = 1; i <= max; i++)
	{
		squareofSum += i;
		sumOfSquare += i * i;
	}

	return (squareofSum * squareofSum) - sumOfSquare;
}

int main()
{
	int max = 100;
	int sumOfSquare = 0;
	int squareofSum = 0;
	std::cout << "Difference " << Difference(max);

	_getch();
	return 0;
}