/*
//Project Euler #12
//https://projecteuler.net/problem=12
*/

#include <iostream>
#include <conio.h>
#include <time.h>

bool IsTriangleNumber(int num, int limit, int& trianlgeNumSum)
{
	int sum = 0;
	int noOfDivisor = 0;

	for(int i = 1; i <= num; i++)
		sum += i;

	for(int i = 1; i <= sum / 2; i++)
	{
		if(sum % i == 0)
		{
			noOfDivisor ++;

			if(noOfDivisor == limit)
			{
				trianlgeNumSum = sum;
				return true;
			}
		}
	}

	return false;
}

int TriangleNumer(int limit)
{
	int num = 1;
	int trianlgeNumSum = 0;

	while(true)
	{
		if(IsTriangleNumber(num, limit, trianlgeNumSum))
			return trianlgeNumSum;
		else 
			num++;
	}

	return 0;
}

int main()
{
	time_t startTime = time(NULL);
	std::cout << "Triangle Number " << TriangleNumer(500);
	std::cout << "\n\nTime taken " << time(NULL) - startTime;

	_getch();
	return 0;
}