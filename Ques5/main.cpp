#include <iostream>
#include <conio.h>

bool IsDivisibleByAll(int var, int max)
{
	for(int i = 1; i <= max; i++)
		if(var % i != 0)
			return false;

	return true;
}

int LargestMultiple(int max)
{
	int counter = max;

	while (true)
	{
		if(IsDivisibleByAll(counter, max))
			return counter;

		counter ++;
	}

	return 0;
}

int main()
{
	std::cout << "Largest Multiple " << LargestMultiple(20);

	_getch();
	return 0;
}