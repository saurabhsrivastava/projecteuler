#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

void getSingleNumber(int num, int& thousands, int& hundred, int& tens, int& ones)
{
	thousands = num / 1000;
	hundred = (num / 100) % 10;
	tens = (num / 10) % 10;
	ones = num % 10;
}

int NumOfDigits(int limit)
{
	int numOfDigit = 0;
	
	string strOnes[] = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
	string strTens[] = { "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
	string strElevenToNineteen[] = { "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
	
	for(int i = 1; i <= limit; i++)
	{
		int thousands = 0;
		int hundreds = 0;
		int tens = 0;
		int ones = 0;

		string numToWord = "";
	
		getSingleNumber(i, thousands, hundreds, tens, ones);

		bool isTensBetween11And19 = (tens == 1 && ones > 0);

		if(thousands > 0)
		{
			numToWord = strOnes[thousands - 1];
			numToWord.append("thousand");
		}

		if(hundreds > 0)
		{
			numToWord.append(strOnes[hundreds - 1]);
			numToWord.append("hundred");

			if(tens > 0 || ones > 0)
				numToWord.append("and");
		}
	
		if(tens > 0)
		{
			numToWord.append(isTensBetween11And19 ? strElevenToNineteen[ones - 1] : strTens[tens - 1]);
			//numToWord.append(" ");
		}

		if(ones > 0)
		{
			//ones
			if(!isTensBetween11And19)
				numToWord.append(strOnes[ones - 1]);
		}

		numOfDigit += numToWord.size();
	}

	return numOfDigit;
}

int main()
{
	cout << "NumOfDigits " << NumOfDigits(1000);

	_getch();
	return 0;
}